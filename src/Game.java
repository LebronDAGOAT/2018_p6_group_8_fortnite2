import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Game extends Application {
	private final Image BRON_IMG = new Image(getClass().getClassLoader().getResource("resources/bron.png").toString());
	private final Image BUSH_IMG = new Image(getClass().getClassLoader().getResource("resources/bush.jpg").toString());
	private final Image TREE_IMG = new Image(getClass().getClassLoader().getResource("resources/tree.png").toString());
	private final Image ROCK_IMG = new Image(getClass().getClassLoader().getResource("resources/rocks.png").toString());
	private final Image CLOUD_IMG = new Image(getClass().getClassLoader().getResource("resources/cloud.jpg").toString());
	private final Image BACKGROUND_IMG = new Image(getClass().getClassLoader().getResource("resources/background.png").toString());
	private final Image SHAQ_IMG = new Image(getClass().getClassLoader().getResource("resources/bulky.jpg").toString());
	private final Image WALL_IMG = new Image(getClass().getClassLoader().getResource("resources/fast.jpg").toString());
	public boolean gameOver = false;
	private GameWorld world;
	public Character customCharacter;
	public BorderPane root;
	public Group background;
	public Stage stage;
	public Scene s;
	public static AnimationTimer obstacleTimer;
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primarystage) throws Exception {
		background = new Group();
		Group initial = new Group();
		root = new BorderPane();
		stage = new Stage();
		stage.setTitle("Obstacle Jumper");
 
		MyAnimationTimer animTimer = new MyAnimationTimer();
		animTimer.start();
		Stage a = new Stage();
		Scene initialScene = new Scene(initial, 700, 350);
		Button fast = new Button();
		Button classical = new Button();
		Button bulky = new Button();
		BorderPane initialRooter = new BorderPane();
		fast.setText("FAST CHARACTER!");
		fast.setPadding(new Insets(15));
		classical.setText("CLASSIC CHARACTER");
		classical.setPadding(new Insets(15));
		bulky.setText("BULKY CHARACTER!");
		bulky.setPadding(new Insets(15));
		HBox boolers = new HBox();
		boolers.setPadding(new Insets(100, 10, 10, 100));
		boolers.getChildren().addAll(fast, classical, bulky); 
		Text textTitle = new Text("                                                                          Obstacle Jumper");
		initialRooter.setTop(textTitle);
		ImageView bulkyViewer = new ImageView(SHAQ_IMG);
		bulkyViewer.setFitHeight(150);
		bulkyViewer.setFitWidth(150);
		HBox imageBox = new HBox();
		ImageView classicView = new ImageView(BRON_IMG);
		ImageView fastView = new ImageView(WALL_IMG);
		fastView.setFitHeight(150);
		fastView.setFitWidth(150);
		classicView.setFitHeight(150);
		classicView.setFitWidth(180);
		imageBox.getChildren().addAll(fastView, classicView, bulkyViewer);
		imageBox.setPadding(new Insets(0, 0, 0, 100));
		initialRooter.setBottom(imageBox);
		initialRooter.setCenter(boolers);
		MyMouseListener mouseListener = new MyMouseListener();
		classical.setOnMousePressed(mouseListener);
		MyBulkyListener bulkyListener = new MyBulkyListener();
		bulky.setOnMousePressed(bulkyListener);
		MyFastListener fastListener = new MyFastListener();
		fast.setOnMousePressed(fastListener);
		initial.getChildren().add(initialRooter);
		
		a.setScene(initialScene);
		a.show();
		  //Scene k = new Scene(, 700, 350);
		int k = 0;
		world = new GameWorld();
		
		
		//stage.setScene(s);
		//gangin
		
		
	}
	
	private void spawnObstacles() {
		obstacleTimer = new AnimationTimer() {
			private long lastUpdate = 0;
			
			@Override
			public void handle(long now) {
				if (now - lastUpdate >= 900_000_000) {
					createRandomObstacle();
					
					lastUpdate = now ;
				}
				
			}
		};
		obstacleTimer.start();
	}
	
	private void createRandomObstacle() {
		Random rand = new Random();
		int selected = rand.nextInt(6);
		int speed = rand.nextInt(3) + 3;
		Obstacle o;
		if (selected == 0) {
			o = new Obstacle(BUSH_IMG, -speed, 0);
			o.setY(215);
			o.setFitHeight(50);    
		} else if (selected == 1) {
			o = new Obstacle(TREE_IMG, -speed, 0);
			o.setY(190);
			o.setFitHeight(80);    
		} else if (selected == 2) {
			o = new Obstacle(ROCK_IMG, -speed, 0);
			o.setY(215);
			o.setFitHeight(50);    
		} else {
			o = new Obstacle(CLOUD_IMG, -speed, 0);
			int value = rand.nextInt(350);
			o.setY((-1 * value) + 100);
			o.setFitHeight(50);
			while (o.getY() < 0) {
				value = rand.nextInt(350);
				o.setY((-1 * value) + 100);
			}	
		}
		o.setFitWidth(50);
		o.setX(700);
		world.add(o);
	}
	
	
	public boolean isGameOver() {
		return gameOver;
	}
	
	public void setGameOver(boolean b) {
		gameOver = b;
	}
	private class MyAnimationTimer extends AnimationTimer{
		private long oldTime = 0;
		private double delay = 1;
		private long diff = System.nanoTime();

		@Override
		public void handle(long newTime) {
			if(newTime - oldTime >= 1e9 * delay){
				oldTime = newTime;
			}
		}
	}
	private class MyMouseListener implements EventHandler<MouseEvent>{

		@Override
		public void handle(MouseEvent event) {
			// TODO Auto-generated method stub
			if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
				customCharacter = new ClassicCharacter(0, 5, BRON_IMG, 210);
				
				customCharacter.setFitHeight(50);
				customCharacter.setFitWidth(40);
				customCharacter.setX(200);
				customCharacter.setY(210);

				world.add(customCharacter);
				world.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().addKeyCode(event.getCode());
					}
				});
				
				world.setOnKeyReleased(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().removeKeyCode(event.getCode());
					}
				}); 

				
				spawnObstacles();
				
				world.start();
				root.setCenter(world);
				//root.setBottom(score);
				background.getChildren().add(new ImageView(BACKGROUND_IMG));
				background.getChildren().add(root);
				Scene s = new Scene(background, 700, 350);
				stage.setScene(s);
				stage.show();
				world.requestFocus();
			}
		}
		
	}
	
	private class MyBulkyListener implements EventHandler<MouseEvent>{

		@Override
		public void handle(MouseEvent event) {
			// TODO Auto-generated method stub
			if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
				customCharacter = new BulkyCharacter(0, 5, SHAQ_IMG, 210);
				
				customCharacter.setFitHeight(50);
				customCharacter.setFitWidth(40);
				customCharacter.setX(200);
				customCharacter.setY(210);

				world.add(customCharacter);
				world.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().addKeyCode(event.getCode());
					}
				});
				
				world.setOnKeyReleased(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().removeKeyCode(event.getCode());
					}
				}); 

				
				spawnObstacles();
				
				world.start();
				root.setCenter(world);
				//root.setBottom(score);
				background.getChildren().add(new ImageView(BACKGROUND_IMG));
				background.getChildren().add(root);
				Scene s = new Scene(background, 700, 350);
				stage.setScene(s);
				stage.show();
				world.requestFocus();
			}
		}
		
	}
	
	private class MyFastListener implements EventHandler<MouseEvent>{

		@Override
		public void handle(MouseEvent event) {
			// TODO Auto-generated method stub
			if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
				customCharacter = new FastCharacter(0, 5, WALL_IMG, 210);
				
				customCharacter.setFitHeight(50);
				customCharacter.setFitWidth(40);
				customCharacter.setX(200);
				customCharacter.setY(210);

				world.add(customCharacter);
				world.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().addKeyCode(event.getCode());
					}
				});
				
				world.setOnKeyReleased(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						// TODO Auto-generated method stub
						customCharacter.getWorld().removeKeyCode(event.getCode());
					}
				}); 

				
				spawnObstacles();
				
				world.start();
				root.setCenter(world);
				//root.setBottom(score);
				background.getChildren().add(new ImageView(BACKGROUND_IMG));
				background.getChildren().add(root);
				Scene s = new Scene(background, 700, 350);
				stage.setScene(s);
				stage.show();
				world.requestFocus();
			}
		}
		
	}
}


