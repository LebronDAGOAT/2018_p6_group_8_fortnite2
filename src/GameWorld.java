import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class GameWorld extends World {

	private final Image GOVER_IMG = new Image(getClass().getClassLoader().getResource("resources/gameOver.png").toString());
	//public boolean gameOver = false;
	private Score score;
	private static int highScore;
	
	public GameWorld() {
		score = new Score();
		score.setX(0);
		score.setY(330);
		score.setScore(0);
		getChildren().add(score);
		highScore = 0;
	}
	
	@Override
	public void act(long now) {
		
	}
	public Score getScore() {
		return score;
	}

	public void gameOver() {
		stop();
		Game.obstacleTimer.stop();
		if(getScore().getScore() > highScore) {
			highScore = getScore().getScore();
		}
		getChildren().remove(score);
		Score hs = new Score();
		hs.setX(300);
		hs.setY(330);
		hs.setText("High Score: " + highScore);
		hs.setFill(Color.WHITE);
		score.setFill(Color.WHITE);
		
		GameOver gOver = new GameOver(GOVER_IMG);
		gOver.setFitHeight(350); 
		gOver.setFitWidth(700);
		gOver.setX(0);
		gOver.setY(0);
		Button exit = new Button("Exit");
		Button restart = new Button("Play Again"); 
		restart.setLayoutX(300); 
		exit.setLayoutX(500);
		
		add(gOver);
		getChildren().add(hs); 
		getChildren().add(score);
		getChildren().add(restart);
		getChildren().add(exit);
		exit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if(e.getSource() == exit) {
					System.exit(0);
				}	
			}
		});
		
		restart.setOnAction(new EventHandler<ActionEvent>() { 
			@Override 
			public void handle(ActionEvent e) { 
				start();
				Game.obstacleTimer.start();
				score.setScore(0);
				score.setFill(Color.RED);
				remove(gOver); getChildren().removeAll(hs, restart, exit); 
				requestFocus();
			} 
		});
	}
}
