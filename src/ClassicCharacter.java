import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

public class ClassicCharacter extends Character{

	private final int JUMP_HEIGHT = 400;
	private double standardY;
	public ClassicCharacter(int x, int y, Image image, double standardYVal) {

		super(x, y, image);
		//standardY = this.getY();
		standardY = standardYVal;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(long now) {
		// TODO Auto-generated method stub
		double yPos = this.getY();
		double xPos = this.getX();
		boolean happening = false;
		GameWorld w = (GameWorld) getWorld();
		w.getScore().setScore(w.getScore().getScore()+1);
		w.getScore().updateDisplay();

		if(getWorld().isKeyDown(KeyCode.SPACE) && getY() > 0){
			
			move(this.getXSpeed(), -this.getYSpeed());

		}else {
			if(this.getY() < standardY) {
				move(this.getXSpeed(), this.getYSpeed());
			}
		}
		
	}
	
	

}
