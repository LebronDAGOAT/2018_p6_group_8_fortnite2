import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

public class BulkyCharacter extends Character{
	
	private double standardY; 

	public BulkyCharacter(int x, int y, Image image, double standardYVal) {
		super(x, y, image);
		standardY = standardYVal;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(long now) {
		// TODO Auto-generated method stub
		double yPos = this.getY();
		double xPos = this.getX();
		boolean happening = false;

		GameWorld w = (GameWorld) getWorld();
		w.getScore().setScore(w.getScore().getScore()+1);
		w.getScore().updateDisplay();

		if(getWorld().isKeyDown(KeyCode.SPACE) && getY() > 0){
			
			move(this.getXSpeed() * 0.4, -this.getYSpeed() * 0.4);

		}else {
			if(this.getY() < standardY) {
				move(this.getXSpeed() * 0.4, this.getYSpeed() * 0.4);
			}
		}
		

		
	}

}
