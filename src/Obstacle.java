import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Obstacle extends Actor {
	double dxSpeed;
	double dySpeed;
	
	public Obstacle(Image img, double xSpeed, double ySpeed) {
		setImage(img);
		this.dxSpeed = xSpeed;
		this.dySpeed = ySpeed;
	}
	
	@Override
	public void act(long now) {

		if (getOneIntersectingObject(Character.class) != null) {
			List<Obstacle> list = getWorld().getObjects(Obstacle.class);
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i) != this && list.get(i) != null) {
					getWorld().remove(list.get(i));
				}
			}
			((GameWorld) getWorld()).gameOver();
			getWorld().remove(this);
			//getWorld().stop();
		} else if (this.getX() <= 0) {
			getWorld().remove(this);
		} else {
			move(dxSpeed, dySpeed); 
		}
	}

}
