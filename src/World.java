import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;

public abstract class World extends javafx.scene.layout.Pane {
	private HashSet<KeyCode> hs;
	private AnimationTimer timer;
	long time = 0;
	public World(){
		hs = new HashSet<>();
		timer = new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				List<Actor> list = getObjects(Actor.class);
				//if (list.size() > 0) {
				//	System.out.println("actor is there");
				//}
				for (int i=  0; i < list.size(); i++) {
					list.get(i).act(now);
				}
			}
		};
	}
	
	public abstract void act(long now);
	
	public void add(Actor actor) {
		this.getChildren().add(actor);
		
	}
	
	public void remove(Actor actor) {
		this.getChildren().remove(actor);
	}
	
	public void start() {
		timer.start();
		List<Actor> list = this.getObjects(Actor.class);
		for (int i = 0; i < list.size(); i++) {
			
			list.get(i).act(time);
		}
	}
	
	public void stop() {
		timer.stop();
	}
	
	public void delay() {
		
	}
	
	public <A extends Actor> java.util.List<A> getObjects(java.lang.Class<A> cls) {
		ArrayList<A> alist = new ArrayList<>();
		ObservableList<Node> list = getChildren();
		for(Node i : list){
			if (cls.isAssignableFrom(i.getClass())){
				alist.add(cls.cast(i));
			}
		}
		//System.out.println(alist);
		return alist;
	}
	
	public void addKeyCode(KeyCode k) {
		hs.add(k);
	}
	
	public void removeKeyCode(KeyCode k) {
		hs.remove(k);
	}
	
	public boolean isKeyDown(KeyCode k) {
		return hs.contains(k);
	}
}