import java.util.ArrayList;

public abstract class Actor extends javafx.scene.image.ImageView {
	private int xPos;
	private int yPos;
	
	public abstract void act(long now);
	
	public void move (double dx, double dy) {
		xPos = (int) getX();
		yPos = (int) getY();
		xPos += dx;
		yPos += dy;
		setX(xPos);
		setY(yPos);
	}
	
	public World getWorld() {
		return (World)getParent();
	}
	
	public double getWidth() {
		return this.getLayoutBounds().getWidth();
	}
	
	public double getHeight() {
		return this.getLayoutBounds().getHeight();
	}
	
	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls) {
		ArrayList<A> li = new ArrayList<>();
		ArrayList<A> objects;
		if(getWorld() != null) {
			objects = (ArrayList<A>) getWorld().getObjects(cls);
			for (int i = 0; i < objects.size(); i++) {
				if (objects.get(i) != this && objects.get(i).intersects(xPos, yPos, getWidth(), getHeight())) {
					li.add(objects.get(i));
				}
			}
			if (li.size() >= 1) {
				return li;
			}
		}
		
		return null;
	}
	
	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls) {
		ArrayList<A> list = (ArrayList<A>) getIntersectingObjects(cls);
		if (list != null && list.size() >= 1) {
			return list.get(0);
		}
		return null;
		
	}
}