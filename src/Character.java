import javafx.scene.image.Image;

public abstract class Character extends Actor {
	private int dxSpeed = 0;
	private int dySpeed = 0;
	//private int jumpHeight;
	
	public Character(int x, int y, Image image){
		dxSpeed = x;
		dySpeed = y;
		this.setImage(image);
	}
	public void setXSpeed(int x){
		dxSpeed = x;
	}
	public void setYSpeed(int y){
		dySpeed = y;
	}
	public void setCharacterImage(Image y){
		this.setImage(y);
	}
	public int getXSpeed(){
		return dxSpeed;
	}
	public int getYSpeed(){
		return dySpeed;
	}
	public Image getCharacterImage(){
		return this.getImage();
	}
	
}


                                                                                     